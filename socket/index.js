var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var redis = require('redis');
var port = process.env.PORT || 5000;

// middleware
io.set('origins', '*:*');

http.listen(port, function() {
	console.log('listening on port '+port);
});

// Store users in connection
var users = [];

// Store messages which sent to user do not connect
var messages = [];

// default: redis run localhost(127.0.0.1), port=6379
// var client = redis.createClient(port, host); customized
var client = redis.createClient(); 

client.on('connect', function() {
    console.log('Redis client connected');
});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

client.get("user:connected", (error, result) => {
	console.log(result);
	if (result !== null)
		users = JSON.parse(result);
});

// Redis test
// client.set('my test key', 'my test value', redis.print);
// client.get('my test key', function (error, result) {
//     if (error) {
//         console.log(error);
//         throw error;
//     }
//     console.log('GET result ->' + result);
// });

// event fired every time a new client connects:
io.on("connection", (socket) => {
	
	// console.log("Socket Server", socket);
	console.info("new client connected");

	socket.on('currentUser', (user) => {
		const currentUser = JSON.parse(user);
		users.push({
			id : socket.id,
			username : currentUser.username,
			role: currentUser.role,
			unit: currentUser.unit
		});

		socket.username = currentUser.username;

		console.log(users);

		let isExistedUserConnect = users.filter(obj => obj.unit === currentUser.unit).length > 0;
		if(!isExistedUserConnect) {
			client.set("user:connected", JSON.stringify(users), redis.print);
		}

		let len = users.length;
		len--;
			
		io.sockets.emit('userList', users, users[len].id); 
	});
		
	// socket.on('getMsg', (data) => {
	// 	socket.broadcast.to(data.toid).emit('sendMsg',{
	// 		msg:data.msg,
	// 		name:data.name
	// 	});
	// });
		
	socket.on('disconnect', function(){
		console.log( socket.name + ' has disconnected from the channel. ' + socket.id);
		for(let i=0; i < users.length; i++){
			if(users[i].id === socket.id){
				users.splice(i,1); 
			}
		}
		console.log("userList after disconnect", users);
		client.set("user:connected", JSON.stringify(users), redis.print);
		io.sockets.emit('exit', users); 
    });

	// TODO: Xu ly nghiep vu 
	// Hai quan
	socket.on('customs-declaration', function(data){
		console.log('A request send from ' + data.user + ' Customs client : ', data.message);
		// socket.emit('company-declaration', { message: 'A new TKThue was created by ' + data.user});
		// console.log("Users:", users);
		let toUsers = users.filter(obj => {
			return (obj.id !== data.socketid && obj.unit === data.unitid);
		});
		// console.log("ToUsers:", toUsers);
		if(toUsers.length > 0) {
			toUsers.forEach(user => {
				let client_socket_id = user.id;
				if(io.sockets.connected[client_socket_id] != undefined){
					console.log(io.sockets.connected[client_socket_id].connected); //or disconected property
					io.to(client_socket_id).emit('company-declaration', { message: 'A new TKThue was created by ' + data.user});
				}else{
					console.log("Socket not connected");
					// TODO: Save data persitence here to notify client disconnect
				}
			});
		} else {
			client.set('customs-sent-messages-' + data.unitid, 
						JSON.stringify({ 
							unit: data.unitid, 
							message: 'A new TKThue was created by ' + data.user,
							status: 'unread'
						}), 
						redis.print);
		}
	});

	socket.on('company-receive-unread-message', (unit, fn) => {
		const connectedUsers = users.filter(obj => obj.unit === unit.unit);
		if(connectedUsers.length > 0) {
			client.get('customs-sent-messages-' + connectedUsers[0].unit, (err, unReadMessages) => {
				if(err) {
					console.log(err); 
					throw err;
				}
				console.log("GET unReadMessages ->" + unReadMessages);
				fn(unReadMessages);
			});
		}
	});

	socket.on('company-received-messages', function(unit){
		console.log("Unitcode: " + unit);
		client.del('customs-sent-messages-' + unit);
	});

	// socket.emit('company-declaration', { message: 'A new TKThue was created!'});

	// Doanh nghiep
	socket.on('company-giaynopthue', function(data){
		console.log('A request send from ' + data.user + ' Company client : ', data.message);
		let toUsers = users.filter(obj => {
			return (obj.id !== data.socketid && obj.unit === data.unitid);
		});
		// console.log("ToUsers:", toUsers);
		if(toUsers.length > 0) {
			toUsers.forEach(user => {
				let client_socket_id = user.id;
				if(io.sockets.connected[client_socket_id] != undefined){
					console.log(io.sockets.connected[client_socket_id].connected); //or disconected property
					io.to(client_socket_id).emit('bank-giaynopthue', { message: 'A new GiayNopThue was created by ' + data.user});
				}else{
					console.log("Socket not connected");
					// TODO: Save data persitence here to notify client disconnect
				}
			});
		} else {
			client.set('company-sent-messages-' + data.unitid, 
						JSON.stringify({ 
							unit: data.unitid, 
							message: 'A new GiayNopThue was created by ' + data.user,
							status: 'unread'
						}), 
						redis.print);
		}
		// socket.broadcast.emit('bank-giaynopthue', { message: 'A new GiayNopThue was created by ' + data.user});
	});

	receivedUnreadMessages(socket, "bank", "company");

	// socket.emit('bank-giaynopthue', { message: 'A new GiayNopThue was created!'});


	// Ngan hang
	socket.on('bank-duyetthanhtoan', function(data){
		console.log('A request send from ' + data.user + ' Bank client : ', data.message);
		let toUsers = users.filter(obj => {
			return (obj.id !== data.socketid && obj.unit === data.unitid);
		});
		// console.log("ToUsers:", toUsers);
		if(toUsers.length > 0) {
			toUsers.forEach(user => {
				let client_socket_id = user.id;
				if(io.sockets.connected[client_socket_id] != undefined){
					console.log(io.sockets.connected[client_socket_id].connected); //or disconected property
					io.to(client_socket_id).emit('customs-duyetthanhtoan', { message: 'A new DuyetThanhToan was created by ' + data.user});
				}else{
					console.log("Socket not connected");
					// TODO: Save data persitence here to notify client disconnect
				}
			});
		} else {
			client.set('bank-sent-messages-' + data.unitid, 
						JSON.stringify({ 
							unit: data.unitid, 
							message: 'A new DuyetThanhToan was created by ' + data.user,
							status: 'unread'
						}), 
						redis.print);
		}
		// socket.broadcast.emit('customs-duyetthanhtoan', { message: 'A new DuyetThanhToan was created by ' + data.user});
	});

	receivedUnreadMessages(socket, "customs", "bank");

	// socket.emit('customs-duyetthanhtoan', { message: 'A new DuyetThanhToan was created!'});


	// Hai quan
	socket.on('customs-xacnhanhtnvt', function(data){
		console.log('A request send from ' + data.user + ' Customs client : ', data.message);
		let toUsers = users.filter(obj => {
			return (obj.id !== data.socketid && obj.unit === data.unitid);
		});
		// console.log("ToUsers:", toUsers);
		if(toUsers.length > 0) {
			toUsers.forEach(user => {
				let client_socket_id = user.id;
				if(io.sockets.connected[client_socket_id] != undefined){
					console.log(io.sockets.connected[client_socket_id].connected); //or disconected property
					io.to(client_socket_id).emit('treasury-xacnhanhtnvt', { message: 'A new XacNhanHTNVT was created by ' + data.user});
				}else{
					console.log("Socket not connected");
					// TODO: Save data persitence here to notify client disconnect
				}
			});
		} else {
			client.set('customs-sent-messages-' + data.unitid, 
						JSON.stringify({ 
							unit: data.unitid, 
							message: 'A new XacNhanHTNVT was created by ' + data.user,
							status: 'unread'
						}), 
						redis.print);
		}
		// socket.broadcast.emit('treasury-xacnhanhtnvt', { message: 'A new XacNhanHTNVT was created by ' + data.user});
	});

	receivedUnreadMessages(socket, "treasury", "customs");

	// socket.emit('treasury-xacnhanhtnvt', { message: 'A new XacNhanHTNVT was created!'});


	// Kho bac
	socket.on('treasury-xacnhannopthue', function(data){
		console.log('A request send from ' + data.user + ' Treasury client : ', data.message);
		let toUsers = users.filter(obj => {
			return (obj.id !== data.socketid && obj.unit === data.unitid);
		});
		// console.log("ToUsers:", toUsers);
		if(toUsers.length > 0) {
			toUsers.forEach(user => {
				let client_socket_id = user.id;
				if(io.sockets.connected[client_socket_id] != undefined){
					console.log(io.sockets.connected[client_socket_id].connected); //or disconected property
					io.to(client_socket_id).emit('customs-xacnhannopthue', { message: 'A new XacNhanNopThue was created by ' + data.user});
				}else{
					console.log("Socket not connected");
					// TODO: Save data persitence here to notify client disconnect
				}
			});
		} else {
			client.set('treasury-sent-messages-' + data.unitid, 
						JSON.stringify({ 
							unit: data.unitid, 
							message: 'A new XacNhanNopThue was created by ' + data.user,
							status: 'unread'
						}), 
						redis.print);
		}
		// socket.broadcast.emit('customs-xacnhannopthue', { message: 'A new XacNhanNopThue was created by ' + data.user});
	});

	receivedUnreadMessages(socket, "customs", "treasury");

	// socket.emit('customs-xacnhannopthue', { message: 'A new XacNhanNopThue was created!'});

//=====================================================================================================
	// io.emit('customs', "message from server to company");
	// socket.on('customs', function(data){
	// 	console.log('request from customs');
	// 	io.emit('company', "Haiquan have created TKThue successfully!");
	// });
	// socket.on('company', function(data){
	// 	console.log('request from company', data);
	// 	io.emit('bank', "message from server to company");
	// });
	// socket.on('bank', function(){
	// 	console.log('request from bank');
	// 	io.emit('customs', "message from server to company");
	// });
	//socket.broadcast.emit('message-a', "message from server to client a");
	//socket.broadcast.emit('message-b', "message from server to client b");
	//socket.broadcast.emit('message-c', "message from server to client c");
	//io.sockets.emit('chat', data);
});

receivedUnreadMessages = (socket, fromRole, toRole) => {
	socket.on(fromRole + '-receive-unread-message', (unit, fn) => {
		const connectedUsers = users.filter(obj => obj.unit === unit.unit);
		if(connectedUsers.length > 0) {
			client.get(toRole + '-sent-messages-' + connectedUsers[0].unit, (err, unReadMessages) => {
				if(err) {
					console.log(err); 
					throw err;
				}
				console.log("GET unReadMessages ->" + unReadMessages);
				fn(unReadMessages);
			});
		}
	});

	socket.on(fromRole + '-received-messages', function(unitCode){
		client.del(toRole + '-sent-messages-' + unitCode);
	});
}