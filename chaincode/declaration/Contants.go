package main

const PING = "pong"
const ELEMENT_PER_PAGE = 5
const INDEX_SUPPORTED = true

// voucher type
const TAX_DECLARATION = "tax_declaration"
const PAYMENT_REQUEST = "payment_request"
const PAYMENT_CONFIRM_REJECT = "payment_confirm_reject"
const TAX_MISSION_CONFIRM = "tax_mission_confirm"
const TREASURY_ACCOUNTING = "treasury_accounting"

// role
const CUSTOMS = "customs"
const COMPANY = "company"
const BANK = "bank"
const TREASURY = "treasury"

// tax declaration status
const CUSTOMS_TAX_DECLARATION = "1"
const COMPANY_PAYMENT_REQUEST = "2"
const BANK_PAYMENT_CONFIRM_REJECT = "3"
const CUSTOMS_TAX_MISSION = "4"
const TREASURY_ACCOUNTING_STT = "5"

// payment request status
const CREATE_NEW = "0"
const APPROVE = "1"
const REJECT = "2"

// incorrect number of args
const INCORRECT_ARGS_1 = "Số lượng tham số không chính xác. Yêu cầu 1!"
const INCORRECT_ARGS_2 = "Số lượng tham số không chính xác. Yêu cầu 2!"
const INCORRECT_ARGS_3 = "Số lượng tham số không chính xác. Yêu cầu 3!"
const INCORRECT_ARGS_4 = "Số lượng tham số không chính xác. Yêu cầu 4!"
const INCORRECT_ARGS_5 = "Số lượng tham số không chính xác. Yêu cầu 5!"
const INCORRECT_ARGS_6 = "Số lượng tham số không chính xác. Yêu cầu 6!"

//message
const CREATED_SUCCESS = "Tạo mới chứng từ thành công!"

//error
const PARAMS_NULL = "Thông tin chứng từ rỗng!"
const CORRUPT_STRUCTURE = "Thông tin chứng từ bị lỗi, vui lòng kiểm tra lại!"
const INVALID_DECLARATION_NUMBER = "Số tờ khai không hợp lệ!"
const INVALID_VOUCHER_NUMBER = "Số chứng từ không hợp lệ!"
const KEY_STATE_EXIST = "Số chứng từ đã tồn tại trên hệ thống!"
const SAVE_ERROR = "Lưu chứng từ không thành công!"
const CONVERTING_ERROR = "Lỗi khi chuyển đổi chứng từ!"
const STORING_ERROR = "Lõi khi lưu chứng từ!"
const GET_QUERY_RESULT_ERROR = "Tìm kiếm không thành công!"
const PAGE_ERROR = "Số trang phải là kiểu số!"