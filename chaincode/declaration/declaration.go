/*
 * Copyright IBM Corp All Rights Reserved
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"regexp"
	"strconv"
	"time"
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/lib/cid"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
)

// using logger so that logs only appear if the ChaincodeLogger LoggingLevel is set to
// LogInfo or LogDebug
var logger = shim.NewLogger("DelclarationChaincode")
var idPattern = regexp.MustCompile("^[0-9]{9}$")

const (
	message       = "Ping"
	StopCharacter = "\r\n\r\n"
)

//==============================================================================================================================
//   Structure Definitions
//==============================================================================================================================
//  Chaincode - A blank struct for use with Shim (A HyperLedger included go file used for get/put state
//        and other HyperLedger functions)
//==============================================================================================================================
type SimpleChaincode struct {
	// CashbackDecimal   uint
	// TokenDecimal      uint
	// TokenSymbol       string
	// TokenName         string
	// TotalSupply       uint64
	// CirculatingSupply uint64
}

// define a tax declaration voucher, create by customs role
type TaxDeclaration struct {
	MA_HQ_PH	string 	`json:"MA_HQ_PH"`	//customsIssueCode
	TEN_HQ_PH	string	`json:"TEN_HQ_PH"`	//customsIssueName
	SO_CT		string	`json:"SO_CT"`		//voucherNumber
	NGAY_HL		string	`json:"NGAY_HL"`	//effectiveDate => time to milliseconds
	MA_HQ		string	`json:"MA_HQ"`		//customsCode
	TEN_HQ		string	`json:"TEN_HQ"`		//customsName
	SO_TK		string	`json:"SO_TK"`		//declarationNumber
	NGAY_DK		string	`json:"NGAY_DK"`	//registerDate => time to milliseconds
	MA_DV		string	`json:"MA_DV"`		//unitCode
	TEN_DV		string	`json:"TEN_DV"`		//unitName
	MA_CHUONG	string	`json:"MA_CHUONG"`	//chapterCode
	MA_LH		string	`json:"MA_LH"`		//typeCode
	TEN_LH		string	`json:"TEN_LH"`		//typeName
	MA_HTVCHH	int		`json:"MA_HTVCHH"`	//transportationCode
	TEN_HTVCHH	string	`json:"TEN_HTVCHH"`	//transportationName
	MA_KB		string	`json:"MA_KB"`		//treasuryCode
	TEN_KB		string	`json:"TEN_KB"`		//treasuryName
	TKKB		string	`json:"TKKB"`		//treasuryAccount
	LOAI_THUE	string	`json:"LOAI_THUE"`	//taxType
	DUNO		float32	`json:"DUNO"`		//debitBalance
	DUNO_TO		float32	`json:"DUNO_TO"`	//debitBalanceSum
	VOUCHER_TYPE 	string	`json:VOUCHER_TYPE`		// voucherType => define in constant [voucher type]
	CREATED_DATE 	string	`json:CREATED_DATE`		// createdDate => time to milliseconds
	LAST_MODIFIED	string	`json:LAST_MODIFIED`	// lastModified => time to milliseconds
	STATUS			string	`json:STATUS`			// status => define in constant [tax declaration status]
}

//==============================================================================================================================
//   read_cert_attribute - Retrieves the attribute name of the certificate.
//          Returns the attribute as a string.
//==============================================================================================================================

func read_cert_attribute(stub shim.ChaincodeStubInterface, name string) (string, error) {
	
	val, ok, err := cid.GetAttributeValue(stub, name)

	if err != nil {
		return "", err
	}
	if !ok {
		return "", errors.New("The attribute is not found")
	}
	return val, nil
}

//==============================================================================================================================
//   check_role - Takes an ecert as a string, decodes it to remove html encoding then parses it and checks the
//              certificates common name. The role is stored as part of the common name.
//==============================================================================================================================

func check_role(stub shim.ChaincodeStubInterface) (string, error) {
	
	role, err := read_cert_attribute(stub, "role")
	if err != nil {
		return "", errors.New("Couldn't get attribute 'role'. Error: " + err.Error())
	}
	return role, nil
}

//==============================================================================================================================
//   get_caller_data - Calls the get_ecert and check_role functions and returns the ecert and role for the
//           name passed.
//==============================================================================================================================

func get_caller_data(stub shim.ChaincodeStubInterface) (string, string, error) {

	mspID, _ := cid.GetMSPID(stub)

	role, err := check_role(stub)

	if err != nil {
		logger.Errorf("Couldn't get caller data, got error: %v", err)
		return "", "", err
	}

	logger.Infof("msp: %s, role: %s", mspID, role)

	return mspID, role, nil
}

//==============================================================================================================================
//  Init Function - Called when the user deploys the chaincode
//==============================================================================================================================
func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

//==============================================================================================================================
//   Router Functions
//==============================================================================================================================
// Invoke is called per transaction on the chaincode. Each transaction is
// either a 'get' or a 'set' on the asset created by Init function. The Set
// method may create a new asset by specifying a new key-value pair.
//==============================================================================================================================
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) peer.Response {

	// Extract the function and args from the transaction proposal
	fn, args := stub.GetFunctionAndParameters()

	result, err := t.invoke(stub, fn, args)

	if err != nil {
		return shim.Error(err.Error())
	}

	// Return the result as success payload
	return shim.Success(result)
}

//==============================================================================================================================
//   Router Functions
//==============================================================================================================================
//  Invoke - Called on chaincode invoke. Takes a function name passed and calls that function. Converts some
//      initial arguments passed to other things for use in the called function e.g. name -> ecert
//  Must check the number of arguments carefully to prevent unwanted messages
//==============================================================================================================================
func (t *SimpleChaincode) invoke(stub shim.ChaincodeStubInterface, function string, args []string) ([]byte, error) {

	// go only runs the selected case, not all the cases that follow, switch case have the same indent
	switch function {

		case "ping":
			return t.ping(stub)

		// case for poc	
		case "poc_delete_state":
			return t.poc_delete_state(stub, args[0])
		
		case "create_new_voucher":
			if len(args) != 2 {
				return nil, errors.New(INCORRECT_ARGS_2)
			}
			return t.create_new_voucher(stub, args[0], args[1])

		case "get_vouchers":
			if len(args) != 3 {
				return nil, errors.New(INCORRECT_ARGS_3)
			}
			return t.get_vouchers(stub, args[0], args[1], args[2])

		case "get_voucher_info":
			if len(args) != 2 {
				return nil, errors.New(INCORRECT_ARGS_2)
			}
			return t.get_voucher_info(stub, args[0], args[1])

		case "customs_get_tax_declarations":
			if len(args) != 6 {
				return nil, errors.New(INCORRECT_ARGS_6)
			}
			return t.customs_get_tax_declarations(stub, args[0], args[1], args[2], args[3], args[4], args[5])

		case "company_get_tax_declarations":
			if len(args) != 5 {
				return nil, errors.New(INCORRECT_ARGS_5)
			}
			return t.company_get_tax_declarations(stub, args[0], args[1], args[2], args[3], args[4])

		case "bank_get_payment_requests":
			if len(args) != 4 {
				return nil, errors.New(INCORRECT_ARGS_4)
			}
			return t.bank_get_payment_requests(stub, args[0], args[1], args[2], args[3])
		case "treasury_get_tax_mission_confirm":
			if len(args) != 4 {
				return nil, errors.New(INCORRECT_ARGS_4)
			}
			return t.treasury_get_tax_mission_confirm(stub, args[0], args[1], args[2], args[3])
		default:
			return nil, errors.New("Funcction of the name " + function + " doesn't exist.")
	}
}

//=================================================================================================================================
//   Ping Function
//=================================================================================================================================
//   Pings the peer to keep the connection alive
//=================================================================================================================================
func (t *SimpleChaincode) ping(stub shim.ChaincodeStubInterface) ([]byte, error) {
	return []byte(PING), nil
}

//=================================================================================================================================
// chaincode for POC
// create a new voucher, define by voucher type
func(t *SimpleChaincode) create_new_voucher(stub shim.ChaincodeStubInterface, voucherType string, params string) ([]byte, error) {
	
	if params != "" {
		switch voucherType {
			case PAYMENT_REQUEST:
				caller, role, _ := get_caller_data(stub)
				if role != COMPANY {
					return nil, errors.New(fmt.Sprintf("Unauthorized: %v", caller))
				}
				return t.create_new_payment_request(stub, params)

			case PAYMENT_CONFIRM_REJECT:
				caller, role, _ := get_caller_data(stub)
				if role != BANK {
					return nil, errors.New(fmt.Sprintf("Unauthorized: %v", caller))
				}
				return t.create_new_payment_confirm_reject(stub, params)

			case TAX_MISSION_CONFIRM:
				caller, role, _ := get_caller_data(stub)
				if role != CUSTOMS {
					return nil, errors.New(fmt.Sprintf("Unauthorized: %v", caller))
				}
				return t.create_new_tax_mission_confirm(stub, params)

			case TREASURY_ACCOUNTING:
				caller, role, _ := get_caller_data(stub)
				if role != TREASURY {
					return nil, errors.New(fmt.Sprintf("Unauthorized: %v", caller))
				}
				return t.create_new_treasury_accounting(stub, params)

			default: //tax_declaration
				caller, role, _ := get_caller_data(stub)
				if role != CUSTOMS {
					return nil, errors.New(fmt.Sprintf("Unauthorized: %v", caller))
				}
				var tax TaxDeclaration
				var err = json.Unmarshal([]byte(params), &tax)
				if err != nil {
					logger.Infof("create_new_voucher: Corrupt tax declaration record: %s, got error: %s", params, err)
					return nil, errors.New(CORRUPT_STRUCTURE)
				}
				declarationNumber := tax.SO_TK
				if declarationNumber == "" {
					logger.Errorf("[create_new_voucher]: Invalid declarationNumber provided: %v", declarationNumber)
					return nil, errors.New(INVALID_DECLARATION_NUMBER)
				}
				voucherNumber := tax.SO_CT
				if voucherNumber == "" {
					logger.Errorf("[create_new_voucher]: Invalid voucherNumber provided: %v", voucherNumber)
					return nil, errors.New(INVALID_VOUCHER_NUMBER)
				}
				record, err := stub.GetState(voucherNumber)
				if record != nil {
					logger.Errorf("Tax declaration already exists %v", voucherNumber)
					return nil, errors.New(KEY_STATE_EXIST)
				}
				tax.VOUCHER_TYPE = TAX_DECLARATION
				tax.CREATED_DATE = strconv.FormatInt(time.Now().UnixNano() / int64(time.Millisecond), 10)
				tax.STATUS = CUSTOMS_TAX_DECLARATION
				tax.LAST_MODIFIED = tax.CREATED_DATE
				_, err = t.tax_declaration_save_changes(stub, tax)
				if err != nil {
					logger.Errorf("[create_new_voucher]: Error saving changes: %s", err)
					return nil, errors.New(SAVE_ERROR)
				}
				return []byte(CREATED_SUCCESS), nil
		}
	} else {
		return nil, errors.New(PARAMS_NULL)
	}
}

//========================================= save voucher function ==================================
// save tax declaration voucher after created
func (t *SimpleChaincode) tax_declaration_save_changes(stub shim.ChaincodeStubInterface, tax TaxDeclaration) (bool, error) {

	bytes, err := json.Marshal(tax)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error converting tax declaration record: %s", err)
		return false, errors.New(CONVERTING_ERROR)
	}

	err = stub.PutState(tax.SO_CT, bytes)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error storing tax declaration record: %s", err)
		return false, errors.New(STORING_ERROR)
	}

	logger.Infof("SAVE_CHANGES: Successfuly storing tax declaration record with key state "+ tax.SO_CT)
	return true, nil
}

//=======================================get vouchers list =======================================
// get all vouchers of declaration
func (t *SimpleChaincode) get_vouchers(stub shim.ChaincodeStubInterface, role string, unit string, declarationNumber string) ([]byte, error) {

	queryString := "{\"selector\":{\"$and\":[";
	
	if role == CUSTOMS {
		queryString += "{\"MA_HQ\":{\"$eq\":\""+unit+"\"}},"
	} else if role == COMPANY {
		queryString += "{\"MA_DV\":{\"$eq\":\""+unit+"\"}},"
	} else if role == BANK {
		companyCode, err := t.bank_get_company_code(stub, unit, declarationNumber)
		if err != nil {
			return nil, err
		}
		queryString += "{\"MA_DV\":{\"$eq\":\""+companyCode+"\"}},"
	} else if role == TREASURY {
		companyCode, err := t.treasury_get_company_code(stub, unit, declarationNumber)
		if err != nil {
			return nil, err
		}
		queryString += "{\"MA_DV\":{\"$eq\":\""+companyCode+"\"}},"
	}
	queryString += "{\"SO_TK\":{\"$eq\":\""+declarationNumber+"\"}},"+
		"{\"CREATED_DATE\":{\"$ne\":\"0\"}}]}"
	if INDEX_SUPPORTED {
		queryString += ",\"sort\":[{\"CREATED_DATE\":\"asc\"}],\"use_index\":[\"_design/payment_request\", \"payment_request-index\"]"
	}
	queryString += "}"

	result, err := t.get_query_result_array(stub, queryString, "0")
	if err != nil {
		return nil, err
	}
	return result, nil
}

// customs get tax declarations voucher list of them
func (t *SimpleChaincode) customs_get_tax_declarations(stub shim.ChaincodeStubInterface, customsCode string, unitCode string, status string, fromDate string, toDate string, page string) ([]byte, error) {
	
	var queryString = "{\"selector\":{\"$and\":["+
		"{\"MA_HQ\":{\"$eq\":\""+customsCode+"\"}},"+
		"{\"VOUCHER_TYPE\":{\"$eq\":\""+TAX_DECLARATION+"\"}},"+
		"{\"LAST_MODIFIED\":{\"$gte\":\""+fromDate+"\"}},{\"LAST_MODIFIED\":{\"$lte\":\""+toDate+"\"}}"
		
	if status != "0" {
		queryString += ",{\"STATUS\":{\"$eq\":\""+status+"\"}}"
	}
	if unitCode != "" {
		queryString += ",{\"MA_DV\":{\"$eq\":\""+unitCode+"\"}}"
	}
	queryString += "]}"
	if INDEX_SUPPORTED {
		queryString += ",\"sort\":[{\"LAST_MODIFIED\":\"desc\"}],\"use_index\":[\"_design/tax_declaration\", \"tax_declaration-index\"]";
	}
	queryString += "}"

	result, err := t.get_query_result_array(stub, queryString, page)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// customs get tax declarations voucher list of them
func (t *SimpleChaincode) company_get_tax_declarations(stub shim.ChaincodeStubInterface, unitCode string, status string, fromDate string, toDate string, page string) ([]byte, error) {
	
	var queryString = "{\"selector\":{\"$and\":["+
		"{\"MA_DV\":{\"$eq\":\""+unitCode+"\"}},"+
		"{\"VOUCHER_TYPE\":{\"$eq\":\""+TAX_DECLARATION+"\"}},"+
		"{\"LAST_MODIFIED\":{\"$gte\":\""+fromDate+"\"}},{\"LAST_MODIFIED\":{\"$lte\":\""+toDate+"\"}}"
		
	if status != "0" {
		queryString += ",{\"STATUS\":{\"$eq\":\""+status+"\"}}"
	}
	queryString += "]}"
	if INDEX_SUPPORTED {
		queryString += ",\"sort\":[{\"LAST_MODIFIED\":\"desc\"}],\"use_index\":[\"_design/tax_declaration\", \"tax_declaration-index\"]";
	}
	queryString += "}"

	result, err := t.get_query_result_array(stub, queryString, page)
	if err != nil {
		return nil, err
	}
	return result, nil
}

//================================ get previous voucher info before create new voucher ================================
// company get tax declaration voucher before create a new payment request voucher
func (t *SimpleChaincode) get_voucher_info(stub shim.ChaincodeStubInterface, voucherType string, declarationNumber string) ([]byte, error) {
	
	var queryString = "{\"selector\":{\"$and\":[{\"VOUCHER_TYPE\":{\"$eq\":\""+voucherType+"\"}},{\"SO_TK\":{\"$eq\":\""+declarationNumber+"\"}}]}}"

	result, err := t.get_query_result_object(stub, queryString)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (t *SimpleChaincode) get_query_result_array(stub shim.ChaincodeStubInterface, queryString string, skip string) ([]byte, error) {
	
	page, err := strconv.Atoi(skip)
	if err != nil {
		return nil, errors.New(PAGE_ERROR)
	}
	if page <= 0 {
		page = 1
	}

	loopCount := 0
	under := (page-1) * ELEMENT_PER_PAGE
	over := page * ELEMENT_PER_PAGE

	logger.Infof("queryString: %s", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	var buffer bytes.Buffer
	bArrayMemberAlreadyWritten := false
	buffer.WriteString("{\"data\":[")
	for resultsIterator.HasNext() {
		loopCount++
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		if loopCount > under && loopCount <= over {
			// Add a comma before array members, suppress it for the first array member
			if bArrayMemberAlreadyWritten == true {
				buffer.WriteString(",")
			}
			// Record is a JSON object in bytes, so we write as-is
			buffer.Write(queryResponse.Value)
			bArrayMemberAlreadyWritten = true
		}
		//  else if loopCount > over {
		// 	break
		// }
	}
	buffer.WriteString("],\"count\":")
	buffer.WriteString(strconv.Itoa(loopCount));
	buffer.WriteString(",\"limit\":")
	buffer.WriteString(strconv.Itoa(ELEMENT_PER_PAGE))
	buffer.WriteString("}")

	return buffer.Bytes(), nil
}

func (t *SimpleChaincode) get_query_result_object(stub shim.ChaincodeStubInterface, queryString string) ([]byte, error) {
	
	logger.Infof("queryString: %s", queryString)

	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	var buffer bytes.Buffer
	if resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}
		// Record is a JSON object in bytes, so we write as-is
		buffer.Write(queryResponse.Value)
	}

	return buffer.Bytes(), nil
}

// update tax declaration status after end user created a new voucher
func (t *SimpleChaincode) update_tax_declaration_status(stub shim.ChaincodeStubInterface, declarationNumber string, status string, dateModified string) (bool, error){

	var tax TaxDeclaration

	bytes, err := t.get_voucher_info(stub, TAX_DECLARATION, declarationNumber)

	if err != nil {
		logger.Infof("[get_tax_declaration]: Error retrieving tax declaration: %s, with declarationNumber = %v", err, declarationNumber)
		return false, errors.New("get_tax_declaration: Error retrieving tax declaration with declarationNumber = " + declarationNumber)
	}

	err = json.Unmarshal(bytes, &tax)
	if err != nil {
		logger.Infof("RETRIEVE_ITEM: Corrupt tax declaration record: %s, got error: %s", bytes, err)
		return false, errors.New("RETRIEVE_ITEM: Corrupt tax declaration record" + string(bytes))
	} else {
		tax.STATUS = status
		tax.LAST_MODIFIED = dateModified
		_, err = t.tax_declaration_save_changes(stub, tax)
		if err != nil {
			logger.Infof("[update_tax_declaration]: Error saving changes: %s", err)
			return false, errors.New("Error saving changes")
		}
		return true, nil
	}
}

//============================= detele state by state key ==================================================
// delete state by key
func (t *SimpleChaincode) poc_delete_state(stub shim.ChaincodeStubInterface, stateKey string) ([]byte, error) {
	
	err := stub.DelState(stateKey)
	
	if err == nil {
		return []byte("delete_successful"), nil
	}
	
	return nil, errors.New("delete failed "+stateKey)
}

//=================================================================================================================================
//   Main - main - Starts up the chaincode
//=================================================================================================================================
func main() {
	// err := shim.Start(&SimpleChaincode {
	// 	CashbackDecimal:   100,
	// 	TokenDecimal:      1000000000,
	// 	TokenSymbol:       "HTN",
	// 	TokenName:         "Hottab Token",
	// 	TotalSupply:       100000000,
	// 	CirculatingSupply: 0,
	// })
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		logger.Infof("Error starting Chaincode: %s", err)
	}
}
