package main

import (
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// define a payment confirm or reject voucher, create by bank role
type PaymentConfirmReject struct {
	DUYET			string	`json:"DUYET"`			//approve
	SO_CT			string 	`json:"SO_CT"`			//voucherNumber
	KYHIEU_CT		string 	`json:"KYHIEU_CT"`		//
	NGAY_CT			string 	`json:"NGAY_CT"`		//voucherDate
	MA_DV			string 	`json:"MA_DV"`			//unitCode
	TEN_DV			string 	`json:"TEN_DV"`			//unitName
	MA_CHUONG		string 	`json:"MA_CHUONG"`		//chapterCode
	MA_HQ			string 	`json:"MA_HQ"`			//customsCode
	TEN_HQ			string 	`json:"TEN_HQ"`			//customsName
	SO_TK			string 	`json:"SO_TK"`			//declarationNumber
	NGAY_DK			string 	`json:"NGAY_DK"`		//registerDate
	MA_LH			string 	`json:"MA_LH"`			//typeCode
	TEN_LH			string 	`json:"TEN_LH"`			//typeName
	MA_KB			string 	`json:"MA_KB"`			//treasuryCode
	TEN_KB			string 	`json:"TEN_KB"`			//treasuryName
	TKKB			string 	`json:"TKKB"`			//treasuryAccount
	SOTIEN_TO		float32	`json:"SOTIEN_TO"`		//
	MA_ST			string 	`json:"MA_ST"`			//
	SOTIEN			float32	`json:"SOTIEN"`			//
	MA_NH_PH		string 	`json:"MA_NH_PH"`		//
	TEN_NH_PH		string 	`json:"TEN_NH_PH"`		//
	MA_NH_TH		string 	`json:"MA_NH_TH"`		//
	TEN_NH_TH		string 	`json:"TEN_NH_TH"`		//
	VOUCHER_TYPE	string	`json:VOUCHER_TYPE`		//
	CREATED_DATE	string	`json:CREATED_DATE`		//
	STATUS			string	`json:STATUS`			//
}

func (t *SimpleChaincode) create_new_payment_confirm_reject(stub shim.ChaincodeStubInterface, params string) ([]byte, error) {
	var payCR PaymentConfirmReject
	var err = json.Unmarshal([]byte(params), &payCR)
	if err != nil {
		logger.Infof("create_new_voucher: Corrupt payment confirm reject record: %s, got error: %s", params, err)
		return nil, errors.New(CORRUPT_STRUCTURE)
	}
	declarationNumber := payCR.SO_TK
	if declarationNumber == "" {
		logger.Errorf("[create_new_voucher]: Invalid declarationNumber provided: %v", declarationNumber)
		return nil, errors.New(INVALID_DECLARATION_NUMBER)
	}
	voucherNumber := payCR.SO_CT
	if voucherNumber == "" {
		logger.Errorf("[create_new_voucher]: Invalid voucherNumber provided: %v", voucherNumber)
		return nil, errors.New(INVALID_VOUCHER_NUMBER)
	}
	record, err := stub.GetState(voucherNumber)
	if record != nil {
		logger.Errorf("payment confirm reject already exists %v", voucherNumber)
		return nil, errors.New(KEY_STATE_EXIST)
	}
	payCR.VOUCHER_TYPE = PAYMENT_CONFIRM_REJECT
	payCR.CREATED_DATE = strconv.FormatInt(time.Now().UnixNano() / int64(time.Millisecond), 10)
	payCR.STATUS = CREATE_NEW
	_, err = t.payment_confirm_reject_save_changes(stub, payCR)
	if err != nil {
		logger.Errorf("[create_new_voucher]: Error saving changes: %s", err)
		return nil, errors.New(SAVE_ERROR)
	}
	return []byte(CREATED_SUCCESS), nil
}

// save payment confirm or reject voucher after created
func (t *SimpleChaincode) payment_confirm_reject_save_changes(stub shim.ChaincodeStubInterface, paymentCR PaymentConfirmReject) (bool, error) {

	bytes, err := json.Marshal(paymentCR)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error converting payment confirm reject record: %s", err)
		return false, errors.New(CONVERTING_ERROR)
	}

	err = stub.PutState(paymentCR.SO_CT, bytes)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error storing payment confirm reject record: %s", err)
		return false, errors.New(STORING_ERROR)
	} else {
		status := APPROVE
		if paymentCR.DUYET == "false" {
			status = REJECT
		}
		_, err = t.update_payment_request_status(stub, paymentCR.SO_TK, status)
		if err == nil {
			logger.Infof("UPDATE_STATUS: Successfuly update status payment request")
		} else {
			logger.Infof("UPDATE_STATUS: Failed update status payment request")
		}
		_, err = t.update_tax_declaration_status(stub, paymentCR.SO_TK, BANK_PAYMENT_CONFIRM_REJECT, paymentCR.CREATED_DATE)
		if err == nil {
			logger.Infof("UPDATE_STATUS: Successfuly update status tax declaration")
		} else {
			logger.Infof("UPDATE_STATUS: Failed update status tax declaration")
		}
	}

	logger.Infof("SAVE_CHANGES: Successfuly storing payment confirm reject record")
	return true, nil
}

func (t *SimpleChaincode) update_payment_confirm_reject_status(stub shim.ChaincodeStubInterface, declarationNumber string, status string) (bool, error){
	
	var paymentCR PaymentConfirmReject

	bytes, err := t.get_voucher_info(stub, PAYMENT_CONFIRM_REJECT, declarationNumber)

	if err != nil {
		logger.Infof("[update_payment_confirm_reject]: Error retrieving payment confirm reject: %s, with declarationNumber = %v", err, declarationNumber)
		return false, errors.New("Error retrieving payment confirm reject with declarationNumber = " + declarationNumber)
	}

	err = json.Unmarshal(bytes, &paymentCR)
	if err != nil {
		logger.Infof("RETRIEVE_ITEM: Corrupt payment confirm reject record: %s, got error: %s", bytes, err)
		return false, errors.New("RETRIEVE_ITEM: Corrupt payment confirm reject record" + string(bytes))
	} else {
		paymentCR.STATUS = status
		_, err = t.payment_confirm_reject_save_changes(stub, paymentCR)
		if err != nil {
			logger.Infof("[update_payment_confirm_reject]: Error saving changes: %s", err)
			return false, errors.New("Error saving changes")
		}
		return true, nil
	}
}