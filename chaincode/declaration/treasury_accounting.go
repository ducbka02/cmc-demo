package main

import (
	"encoding/json"
	"errors"
	"strconv"
	"time"

	"github.com/hyperledger/fabric/core/chaincode/shim"
)

// define a treasury accounting voucher, create by treasury role
type TreasuryAccounting struct {
	SO_CT		string	`json:"SO_CT"`
	KYHIEU_CT	string	`json:"KYHIEU_CT"`
	NGAY_CT		string	`json:"NGAY_CT"`
	MA_KB		string	`json:"MA_KB"`
	TEN_KB		string	`json:"TEN_KB"`
	MA_DV		string	`json:"MA_DV"`
	TEN_DV		string	`json:"TEN_DV"`
	SO_TK		string	`json:"SO_TK"`
	NAM_DK		string	`json:"NAM_DK"`
	MA_NDKT		string	`json:"MA_NDKT"`
	NDKT		string	`json:"NDKT"`
	VOUCHER_TYPE string	`json:VOUCHER_TYPE`
	CREATED_DATE string `json:CREATED_DATE`
	STATUS		string	`json:STATUS`
	MA_HQ		string	`json:MA_HQ`
}

func (t *SimpleChaincode) create_new_treasury_accounting(stub shim.ChaincodeStubInterface, params string) ([]byte, error) {
	var treasuryAcc TreasuryAccounting
	var err = json.Unmarshal([]byte(params), &treasuryAcc)
	if err != nil {
		logger.Infof("create_new_voucher: Corrupt treasury accounting record: %s, got error: %s", params, err)
		return nil, errors.New(CORRUPT_STRUCTURE)
	}
	declarationNumber := treasuryAcc.SO_TK
	if declarationNumber == "" {
		logger.Errorf("[create_new_voucher]: Invalid declarationNumber provided: %v", declarationNumber)
		return nil, errors.New(INVALID_DECLARATION_NUMBER)
	}
	voucherNumber := treasuryAcc.SO_CT
	if voucherNumber == "" {
		logger.Errorf("[create_new_voucher]: Invalid voucherNumber provided: %v", voucherNumber)
		return nil, errors.New(INVALID_VOUCHER_NUMBER)
	}
	record, err := stub.GetState(voucherNumber)
	if record != nil {
		logger.Errorf("treasury accounting already exists %v", voucherNumber)
		return nil, errors.New(KEY_STATE_EXIST)
	}
	treasuryAcc.VOUCHER_TYPE = TREASURY_ACCOUNTING
	treasuryAcc.CREATED_DATE = strconv.FormatInt(time.Now().UnixNano() / int64(time.Millisecond), 10)
	treasuryAcc.STATUS = CREATE_NEW
	_, err = t.treasury_accounting_save_changes(stub, treasuryAcc)
	if err != nil {
		logger.Errorf("[create_new_voucher]: Error saving changes: %s", err)
		return nil, errors.New(SAVE_ERROR)
	}
	return []byte(CREATED_SUCCESS), nil
}

// save treasury accounting voucher after created
func (t *SimpleChaincode) treasury_accounting_save_changes(stub shim.ChaincodeStubInterface, treasuryAccounting TreasuryAccounting) (bool, error) {

	bytes, err := json.Marshal(treasuryAccounting)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error converting treasury accounting record: %s", err)
		return false, errors.New(CONVERTING_ERROR)
	}

	err = stub.PutState(treasuryAccounting.SO_CT, bytes)

	if err != nil {
		logger.Infof("SAVE_CHANGES: Error storing treasury accounting record: %s", err)
		return false, errors.New(STORING_ERROR)
	} else {
		status := APPROVE
		_, err = t.update_tax_mission_confirm_status(stub, treasuryAccounting.SO_TK, status)
		if err == nil {
			logger.Infof("UPDATE_STATUS: Successfuly update status payment confirm reject")
		} else {
			logger.Infof("UPDATE_STATUS: Failed update status payment confirm reject")
		}

		_, err = t.update_tax_declaration_status(stub, treasuryAccounting.SO_TK, TREASURY_ACCOUNTING_STT, treasuryAccounting.CREATED_DATE)
		if err == nil {
			logger.Infof("UPDATE_STATUS: Successfuly update status tax declaration")
		} else {
			logger.Infof("UPDATE_STATUS: Failed update status tax declaration")
		}
	}

	logger.Infof("SAVE_CHANGES: Successfuly storing treasury accounting record")
	return true, nil
}

func (t *SimpleChaincode) treasury_get_company_code(stub shim.ChaincodeStubInterface, treasuryCode string, declarationNumber string) (string, error) {
	
	var queryString = "{\"selector\":{\"$and\":["+
		"{\"MA_KB\":{\"$eq\":\""+treasuryCode+"\"}},"+
		"{\"SO_TK\":{\"$eq\":\""+declarationNumber+"\"}},"+
		"{\"VOUCHER_TYPE\":{\"$eq\":\""+TAX_DECLARATION+"\"}}]}}"

	result, err := t.get_query_result_object(stub, queryString)
	if err != nil {
		return "", err
	}

	var taxDeclaration TaxDeclaration
	err = json.Unmarshal(result, &taxDeclaration)
	if err != nil {
		return "error", err
	}
	companyCode := taxDeclaration.MA_DV
	return companyCode, nil
}