package main

import (
    //"encoding/json"
    "fmt"
    "testing"
    "github.com/hyperledger/fabric/core/chaincode/shim"
)

func test (t *testing.T) {
    fmt.Println("Entering Test declaration chaincode")
    attributes := make(map[string][]byte)
    //Create a custom MockStub that internally uses shim.MockStub
    stub := shim.NewCustomMockStub("mockStub", new(SimpleChaincode), attributes)
    if stub == nil {
        t.Fatalf("MockStub creation failed")
    }
}