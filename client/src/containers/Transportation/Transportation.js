import React, { Component } from 'react';
import './Transportation.css';
import Company from '../../components/Company/Company';
import CompanyCreate from '../../components/Company/CompanyCreate/CompanyCreate';
import CompanyUpdate from '../../components/Company/CompanyUpdate/CompanyUpdate';
import CompanyHistory from '../../components/Company/CompanyHistory/CompanyHistory';
import Contract from '../../components/Contract/Contract';
import ContractCreate from '../../components/Contract/ContractCreate/ContractCreate';
import ContractUpdate from '../../components/Contract/ContractUpdate/ContractUpdate';
import ContractHistory from '../../components/Contract/ContractHistory/ContractHistory';
import Transaction from '../../components/Transaction/Transaction';
import TransactionHistory from '../../components/Transaction/TransactionHistory/TransactionHistory';
import { Route, NavLink, Switch, Redirect } from 'react-router-dom';

class Transportation extends Component {
    render() {
        return (
            <div className="Transportation">
                <header>
                    <nav>
                        <ul>
                            <li><NavLink to="/contract" exact activeClassName="active" activeStyle={{ color: 'blue' }}>Hợp đồng</NavLink></li>
                            <li><NavLink to="/company" exact activeClassName="active" activeStyle={{ color: 'blue' }}>Doanh nghiệp</NavLink></li>
                        </ul>
                    </nav>
                </header>
                <Switch>
                    <Route path="/company/:id/trans" component={Transaction} />
                    <Route path="/company/trans/:id" component={TransactionHistory} />

                    <Route path="/company/new" component={CompanyCreate} />
                    <Route path="/company/history/:id" component={CompanyHistory} />
                    <Route path="/company/update/:id" component={CompanyUpdate} />
                    <Route path="/company" component={Company} />

                    <Route path="/contract/new" component={ContractCreate} />
                    <Route path="/contract/history/:id" component={ContractHistory} />
                    <Route path="/contract/update/:id" component={ContractUpdate} />
                    <Route path="/contract" component={Contract} />

                    <Redirect from="/" to="/contract/new" />
                </Switch>
                
            </div>
        );
    }
}

export default Transportation;