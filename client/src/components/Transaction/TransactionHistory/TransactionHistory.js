import React, { Component } from 'react';
import './TransactionHistory.css';
import { Table } from 'react-bootstrap';
import Spinner from '../../UI/Spinner/Spinner';
import { query } from "../../../api";

class TransactionHistory extends Component {

    state = {
        history: [],
        loading: false
    };

    componentDidMount() {
        this.setState({ loading: true });
        const id = this.props.match.params.id;
        query("poc_get_transaction", id).then(history => {
            this.setState({ history: history, loading: false});
        });
    }

    render() {
        // const errorText = (
        //     <tr>
        //         <td colSpan="4" style={{ color: 'red' }}>Không lấy được dữ liệu!!!</td>
        //     </tr>
        // );
        let orderInfo = null;
        
        let infoSummary = null;

        if(this.state.history) {
            orderInfo = (
                this.state.history.map( (obj, index) => {
                    return (
                        <tr key={index + 1}>
                            <td>{index + 1}</td>
                            <td>{obj.TRANS_ID}</td>
                            <td>{obj.SO_HD}</td>
                            <td>{obj.MA_NGAN_HANG}</td>
                            <td>{obj.NGAY_THANH_TOAN}</td>
                            <td>{obj.BEN_NHAN}</td>
                            <td>{obj.LAN_THANH_TOAN}</td>
                            <td>{obj.SO_TIEN}</td>
                        </tr>
                    );
                })
            );
        }

        if(!this.state.loading) {
            infoSummary = (
                <div className="Info">
                    <h1>Thông Tin Giao Dịch Hợp Đồng</h1>
                    <Table striped bordered condensed hover responsive className="TableInfo">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>TRANS_ID</th>
                            <th>SO_HD</th>
                            <th>MA_NGAN_HANG</th>
                            <th>NGAY_THANH_TOAN</th>
                            <th>BEN_NHAN</th>
                            <th>LAN_THANH_TOAN</th>
                            <th>SO_TIEN</th>
                        </tr>
                        </thead>
                        <tbody>
                            {orderInfo}
                        </tbody>
                    </Table>
                </div>
            );
        } else {
            infoSummary = <Spinner />;
        }

        

        return (
            <div>
                {infoSummary}
            </div>
        );
    }
}

export default TransactionHistory;