import React, { Component } from 'react';
import './Login.css';
import axios from 'axios';
import { SERVER_URL } from '../../../api_server';

class Login extends Component {
    state = {
        username: '',
        password: '',
        message: ''
    };

    submitLoginHandler = (event) => {
        event.preventDefault();
        const data = {
            username: this.state.username,
            password: this.state.password
        };
        axios.post(`${SERVER_URL}/auth/login`, data)
            .then(res => {
                // console.log(res);
                if(res.data.length > 0) {
                    this.props.history.push('/home');
                } else {
                    this.setState({ message: 'Invalid username or password!'});
                }
            })
            .catch(err => {
                console.log(err);
            });
    }

    render() {
        return (
            <div className="login-page">
                <div className="form">
                <form className="login-form" onSubmit={this.submitLoginHandler}>
                <input type="text" 
                    value={this.state.username} 
                    onChange={(e) => this.setState({username: e.target.value})} 
                    placeholder="username"/>
                <input type="password" 
                    value={this.state.password} 
                    onChange={(e) => this.setState({password: e.target.value})} 
                    placeholder="password"/>
                    <button>login</button>
                <p className="error-message">{this.state.message}</p>
                </form>
                </div>
            </div>
        );
    }
}

export default Login;