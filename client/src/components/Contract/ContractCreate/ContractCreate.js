import React, { Component } from 'react';
import './ContractCreate.css';
import Spinner from '../../UI/Spinner/Spinner';
import { Form, Col, FormGroup, FormControl, ControlLabel, Button, ButtonToolbar } from 'react-bootstrap';
import { invoke } from "../../../api";

class ContractCreate extends Component {
    state = {
        SO_HD: '',
        NGAY_HD: '',
        NGAY_TAO: '',
        MST_BEN_A: 0,
        MST_BEN_B: 0,
        SO_TIEN: 0,
        VAT: 0,
        TONG_TIEN: 0,
        NGUOI_TAO: '',
        TRANG_THAI: '',
        submitted: false,
        loading: false
    };

    orderHandler = (event) => {
        event.preventDefault();
        this.setState( { loading: true } );
        const data = {
            SO_HD: this.state.SO_HD,
            NGAY_HD: this.state.NGAY_HD,
            NGAY_TAO: this.state.NGAY_TAO,
            MST_BEN_A: parseInt(this.state.MST_BEN_A),
            MST_BEN_B: parseInt(this.state.MST_BEN_B),
            SO_TIEN: parseInt(this.state.SO_TIEN),
            VAT: parseInt(this.state.VAT),
            TONG_TIEN: parseInt(this.state.TONG_TIEN),
            NGUOI_TAO: this.state.NGUOI_TAO,
            TRANG_THAI: this.state.TRANG_THAI
        };
        // console.log(data);
        invoke("poc_create_contract", JSON.stringify(data)).then(data => {
            this.setState({ loading: false });
            this.props.history.push(`/contract`);
        });
    }

    render() {  
        let submittedOrder = null;

        if(this.state.loading) {
            submittedOrder = <Spinner />;
        } else {
            submittedOrder = (
                <div className="NewCompany">
                    <h1>Tạo Mới Hợp Đồng</h1>
                    <Form horizontal onSubmit={this.orderHandler} className="FormCompany">
                        <FormGroup controlId="formHorizontalSO_HD">
                            <Col componentClass={ControlLabel} sm={2}>
                                Số HĐ:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.SO_HD} placeholder="Enter SO_HD" 
                                onChange={ (event) => this.setState({SO_HD: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalNGAY_HD">
                            <Col componentClass={ControlLabel} sm={2}>
                                Ngày HĐ:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.NGAY_HD} placeholder="Enter NGAY_HD" 
                                onChange={ (event) => this.setState({NGAY_HD: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalNGAY_TAO">
                            <Col componentClass={ControlLabel} sm={2}>
                                Ngày Tạo:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.NGAY_TAO} placeholder="Enter NGAY_TAO" 
                                onChange={ (event) => this.setState({NGAY_TAO: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalMST_BEN_A">
                            <Col componentClass={ControlLabel} sm={2}>
                                MST Bên A:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.MST_BEN_A} placeholder="Enter MST_BEN_A" 
                                onChange={ (event) => this.setState({MST_BEN_A: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalMST_BEN_B">
                            <Col componentClass={ControlLabel} sm={2}>
                                MST Bên B:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.MST_BEN_B} placeholder="Enter MST_BEN_B" 
                                onChange={ (event) => this.setState({MST_BEN_B: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalSO_TIEN">
                            <Col componentClass={ControlLabel} sm={2}>
                                Số Tiền:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.SO_TIEN} placeholder="Enter SO_TIEN" 
                                onChange={ (event) => this.setState({SO_TIEN: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalVAT">
                            <Col componentClass={ControlLabel} sm={2}>
                                VAT:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.VAT} placeholder="Enter VAT" 
                                onChange={ (event) => this.setState({VAT: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalTONG_TIEN">
                            <Col componentClass={ControlLabel} sm={2}>
                                Tổng Tiền:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.TONG_TIEN} placeholder="Enter TONG_TIEN" 
                                onChange={ (event) => this.setState({TONG_TIEN: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalNGUOI_TAO">
                            <Col componentClass={ControlLabel} sm={2}>
                                Người Tạo:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.NGUOI_TAO} placeholder="Enter NGUOI_TAO" 
                                onChange={ (event) => this.setState({NGUOI_TAO: event.target.value}) } />
                            </Col>
                        </FormGroup>

                        <FormGroup controlId="formHorizontalTRANG_THAI">
                            <Col componentClass={ControlLabel} sm={2}>
                                Trạng Thái:
                            </Col>
                            <Col sm={10}>
                            <FormControl type="text" value={this.state.TRANG_THAI} placeholder="Enter TRANG_THAI" 
                                onChange={ (event) => this.setState({TRANG_THAI: event.target.value}) } />
                            </Col>
                        </FormGroup>
                        
                        <ButtonToolbar className="ButtonToolBar">
                            <Button bsStyle="primary" onClick={this.orderHandler}>Tạo Mới</Button>
                        </ButtonToolbar>
                    </Form>
                </div>
            );
        }

        return (
            <div>
                {submittedOrder}
            </div>
        );
    }
}

export default ContractCreate;