import React, { Component } from 'react';
import './Contract.css';
import { Table } from 'react-bootstrap';
import Spinner from '../UI/Spinner/Spinner';
import { NavLink } from 'react-router-dom';
import { Button, ButtonToolbar, Glyphicon } from 'react-bootstrap';
import { query } from "../../api";

class Contract extends Component {

    state = {
        contracts: [],
        loading: false
    };

    componentDidMount() {
        this.setState({ loading: true });
        query("poc_get_all_assets", "contract").then(contracts => {
            this.setState({ contracts: contracts, loading: false});
        });
    }

    createCompanyHandler = () => {
        this.props.history.push(`/contract/new`);
    }

    render() {
        // const errorText = (
        //     <tr>
        //         <td colSpan="4" style={{ color: 'red' }}>Không lấy được dữ liệu!!!</td>
        //     </tr>
        // );
        let companyInfo = null;
        
        let infoSummary = null;

        if(this.state.contracts) {
            companyInfo = (
                this.state.contracts.map( (obj, index) => {
                    return (
                        <tr key={index + 1}>
                            <td>{index + 1}</td>
                            <td>{obj.SO_HD}</td>
                            <td>{obj.NGAY_HD}</td>
                            <td>{obj.NGAY_TAO}</td>
                            <td>{obj.MST_BEN_A}</td>
                            <td>{obj.MST_BEN_B}</td>
                            <td>{obj.SO_TIEN}</td>
                            <td>{obj.VAT}</td>
                            <td>{obj.TONG_TIEN}</td>
                            {/* <td>{obj.REF}</td> */}
                            <td>{obj.NGUOI_TAO}</td>
                            <td>{obj.TRANG_THAI}</td>
                            <td>
                                <NavLink to={`/contract/update/${obj.SO_HD}`}
                                    exact activeClassName="active" 
                                    activeStyle={{ color: 'blue' }}><Glyphicon glyph="edit" style={{ color: 'red' }} /></NavLink>
                                <NavLink to={`/contract/history/${obj.SO_HD}`}
                                    exact activeClassName="active" 
                                    activeStyle={{ color: 'blue' }}><Glyphicon glyph="bitcoin" style={{ color: 'yellow' }} /></NavLink>
                                <NavLink to={`/company/${obj.SO_HD}/trans`}
                                    exact activeClassName="active" 
                                    activeStyle={{ color: 'blue' }}><Glyphicon glyph="plus" style={{ color: 'green' }} /></NavLink>
                                <NavLink to={`/company/trans/${obj.SO_HD}`}
                                    exact activeClassName="active" 
                                    activeStyle={{ color: 'blue' }}><Glyphicon glyph="file" style={{ color: 'orange' }} /></NavLink>
                            </td>
                        </tr>
                        
                    );
                })
            );
        }

        if(!this.state.loading) {
            infoSummary = (
                
                        <Table striped bordered condensed hover responsive className="TableInfo">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>SO_HD</th>
                                <th>NGAY_HD</th>
                                <th>NGAY_TAO</th>
                                <th>MST_BEN_A</th>
                                <th>MST_BEN_B</th>
                                <th>SO_TIEN</th>
                                <th>VAT</th>
                                <th>TONG_TIEN</th>
                                {/* <th>REF</th> */}
                                <th>NGUOI_TAO</th>
                                <th>TRANG_THAI</th>
                                <th>ACTIONS</th>
                            </tr>
                            </thead>
                            <tbody>
                                {companyInfo}
                            </tbody>
                        </Table>
                    
            );
        } else {
            infoSummary = <Spinner />;
        }

        

        return (
            <div> 
                <div className="Info">
                    <h1>Danh Sách Hợp Đồng</h1>
                    {infoSummary}
                </div>
                <ButtonToolbar className="ButtonToolBar">
                       <Button bsStyle="success" onClick={this.createCompanyHandler}><Glyphicon glyph="plus" />Tạo Mới</Button>
                </ButtonToolbar>
            </div>
        );
    }
}

export default Contract;