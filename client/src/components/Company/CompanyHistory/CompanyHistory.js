import React, { Component } from 'react';
import './CompanyHistory.css';
import { Table } from 'react-bootstrap';
import Spinner from '../../UI/Spinner/Spinner';
import { query } from "../../../api";

class CompanyHistory extends Component {

    state = {
        history: [],
        loading: false
    };

    componentDidMount() {
        this.setState({ loading: true });
        const id = this.props.match.params.id;
        query("get_history", "corporate", id, "10").then(history => {
            this.setState({ history: history, loading: false});
        });
    }

    render() {
        let orderInfo = null;
        
        let infoSummary = null;

        if(this.state.history) {
            orderInfo = (
                this.state.history.map( (history, index) => {
                    return (
                        <tr key={index + 1}>
                            <td>{index + 1}</td>
                            {/* <td>{history.TxId}</td> */}
                            <td>{history.Value.MA_SO_THUE}</td>
                            <td>{history.Value.TEN_DN}</td>
                            <td>{history.Value.MA_NGAN_HANG}</td>
                            <td>{history.Value.TEN_NGAN_HANG}</td>
                            <td>{history.Value.SO_TAI_KHOAN}</td>
                            <td>{history.Value.TEN_TAI_KHOAN}</td>
                            <td>{history.Timestamp}</td>
                        </tr>
                    );
                })
            );
        }

        if(!this.state.loading) {
            infoSummary = (
                <div className="Info">
                    <h1>Thông Tin Giao Dịch Doanh Nghiệp</h1>
                    <Table striped bordered condensed hover responsive className="TableInfo">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>MA_SO_THUE</th>
                            <th>TEN_DN</th>
                            <th>MA_NGAN_HANG</th>
                            <th>TEN_NGAN_HANG</th>
                            <th>SO_TAI_KHOAN</th>
                            <th>TEN_TAI_KHOAN</th>
                            <th>Timestamp</th>
                        </tr>
                        </thead>
                        <tbody>
                            {orderInfo}
                        </tbody>
                    </Table>
                </div>
            );
        } else {
            infoSummary = <Spinner />;
        }

        

        return (
            <div>
                {infoSummary}
            </div>
        );
    }
}

export default CompanyHistory;