import React, { Component } from "react";
import "./Declaration.css";
import {
  FormGroup,
  span,
  FormControl,
  Col,
  Button,
  Alert,
  Table
} from "react-bootstrap";
import Spinner from "../UI/Spinner/Spinner";
import DatePicker from "react-datepicker";
import moment from "moment";
import NumberFormat from "react-number-format";

import "react-datepicker/dist/react-datepicker.css";

import { query, invoke } from "../../api";
import axios from "axios";
import { SERVER_URL, SOCKET_URL } from "../../api_server";
import openSocket from "socket.io-client";

class GiayNopThue extends Component {
  state = {
    MA_HQ_PH: "",
    TEN_HQ_PH: "",
    SO_CT_HQ: 0,
    SO_CT: 0,
    NGAY_HL: moment(),
    MA_HQ: "",
    TEN_HQ: "",
    SO_TK: 0,
    NGAY_DK: moment(),
    MA_DV: "",
    TEN_DV: "",
    MA_CHUONG: "",
    MA_LH: "",
    TEN_LH: "",
    MA_HTVCHH: 0,
    TEN_HTVCHH: "",
    MA_KB: "",
    TEN_KB: "",
    TKKB: "",
    LOAI_THUE: "",
    DUNO: 0,
    DUNO_TO: 0,
    MONEY_ST1: 0,
    MONEY_ST2: 0,
    MONEY_ST3: 0,
    MONEY_ST4: 0,
    MONEY_ST5: 0,
    value: "",
    TenHQPH: [],
    TenHQ: [],
    SacThue: [],
    TenDVXNK: [],
    LHXNK: [],
    TenNH: [],
    KhoBac: [],
    MA_NH: "",
    TEN_NH: "",
    NGAY_LAP: moment(),
    SO_TK: "",
    MA_TK_PH: "",
    TEN_TK_PH: "",
    TEN_TK: "",
    SO_TIEN: "",
    KHCT: "",
    ST: [], // Cac loai Sac Thue
    message: "",
    loading: false,
    isShow: false
  };

  componentDidMount() {
    const SO_TK = new URLSearchParams(this.props.location.search).get("id");
    query("get_voucher_info", this.props.currentUser, "tax_declaration", SO_TK)
      .then(res => {
        console.log(res);
        this.setState({
          SO_TK: SO_TK,
          SO_CT_HQ: res.SO_CT,
          MA_HQ_PH: res.MA_HQ_PH,
          TEN_HQ_PH: res.TEN_HQ_PH,
          MA_HQ: res.MA_HQ,
          TEN_HQ: res.TEN_HQ,
          MA_DV: res.MA_DV,
          TEN_DV: res.TEN_DV,
          MA_LH: res.MA_LH,
          TEN_LH: res.TEN_LH,
          MA_KB: res.MA_KB,
          TEN_KB: res.TEN_KB,
          MA_HTVCHH: res.MA_HTVCHH,
          TEN_HTVCHH: res.TEN_HTVCHH,
          TKKB: res.TKKB,
          DUNO_TO: res.DUNO_TO,
          LOAI_THUE: res.LOAI_THUE,
          NGAY_DK: moment(res.NGAY_DK, "DD/MM/YYYY"),
          NGAY_HL: moment(res.NGAY_HL, "DD/MM/YYYY"),
          MA_CHUONG: res.MA_CHUONG,
          ST: JSON.parse(res.LOAI_THUE).filter(obj => obj.ST_MONEY !== 0)
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=bank_list`)
      .then(res => {
        console.log(res);
        this.setState({
          TenNH: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=customs_unit`)
      .then(res => {
        this.setState({
          TenHQPH: res.data,
          TenHQ: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=tax_type`)
      .then(res => {
        this.setState({
          SacThue: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=company_list`)
      .then(res => {
        this.setState({
          TenDVXNK: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=type_code`)
      .then(res => {
        this.setState({
          LHXNK: res.data
        });
      })
      .catch(err => console.log(err));

    axios
      .get(`${SERVER_URL}/api/choicelist?choicelistType=treasury_list`)
      .then(res => {
        this.setState({
          KhoBac: res.data
        });
      })
      .catch(err => console.log(err));
  }

  handleChangeTenNH = e => {
    this.setState({
      MA_NH: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.setState({ loading: true });
    const data = {
      // MA_HQ_PH: this.state.MA_HQ_PH,
      // TEN_HQ_PH: this.state.TEN_HQ_PH,
      // NGAY_HL: moment(this.state.NGAY_HL).format("DD/MM/YYYY"),
      // MA_HTVCHH: this.state.MA_HTVCHH,
      // TEN_HTVCHH: this.state.TEN_HTVCHH,
      SO_CT: this.state.SO_CT,
      KYHIEU_CT: this.state.KHCT,
      NGAY_CT: moment(this.state.NGAY_LAP).format("DD/MM/YYYY"),
      MA_DV: this.state.MA_DV,
      TEN_DV: this.state.TEN_DV,
      MA_CHUONG: this.state.MA_CHUONG,
      MA_HQ: this.state.MA_HQ,
      TEN_HQ: this.state.TEN_HQ,
      SO_TK: this.state.SO_TK,
      NGAY_DK: moment(this.state.NGAY_DK).format("DD/MM/YYYY"),
      MA_LH: this.state.MA_LH,
      TEN_LH: this.state.TEN_LH,
      MA_KB: this.state.MA_KB,
      TEN_KB: this.state.TEN_KB,
      TKKB: this.state.TKKB,
      SOTIEN_TO: this.state.DUNO_TO,
      MA_ST: this.state.LOAI_THUE,
      SOTIEN: parseInt(this.state.SO_TIEN, 10),
      MA_NH_PH: this.state.MA_NH,
      TEN_NH_PH: this.state.TEN_NH,
      TAIKHOAN_PH: this.state.MA_TK_PH,
      TEN_TAIKHOAN_PH: this.state.TEN_TK_PH
    };

    console.log(data);

    // const socket = openSocket(SOCKET_URL);

    invoke(
      "create_new_voucher",
      this.props.currentUser,
      "payment_request",
      JSON.stringify(data)
    )
      .then(res => {
        // console.log(res);
        if (res.code == 2) { // Catch error from server
          this.setState({
            loading: false,
            message: res.details.split(":")[2].replace(")", ""),
            isShow: true
          });
        } else {
          if (res[0].status === "SUCCESS") {
            this.setState({
              loading: false,
              message: "SUCCESS",
              isShow: true
            });
            // Send message to Server
            this.props.socket.emit("company-giaynopthue", { 
              message: "Hey, Company have created GiayNopThue successfully!",
              socketid: this.props.socket.id,
              user: localStorage.getItem("username"),
              role: localStorage.getItem("role"),
              unitid: this.state.MA_NH
            });
          } else {
            // console.log(res.details)
            this.setState({
              loading: false,
              message: "ERROR",
              isShow: true
            });
          }
        }
        
      })
      .catch(err => {
        console.log(err);
        this.setState({
          loading: false,
          message: "ERROR",
          isShow: true
        });
      });
  };

  // handleClickCancel = () => {
  //   this.setState({
  //     MA_HQ_PH: "",
  //     TEN_HQ_PH: "",
  //     SO_CT: 0,
  //     NGAY_HL: moment(),
  //     MA_HQ: "",
  //     TEN_HQ: "",
  //     SO_TK: 0,
  //     NGAY_DK: moment(),
  //     MA_DV: "",
  //     TEN_DV: "",
  //     MA_CHUONG: "",
  //     MA_LH: "",
  //     TEN_LH: "",
  //     MA_HTVCHH: 0,
  //     TEN_HTVCHH: "",
  //     MA_KB: "",
  //     TEN_KB: "",
  //     TKKB: "",
  //     LOAI_THUE: "",
  //     DUNO: 0,
  //     DUNO_TO: 0
  //   });
  // };

  handleDismissAlert = () => {
    this.setState({ isShow: false });
  };

  handleChangeNgayLAP = date => {
    this.setState({
      NGAY_LAP: date
    });
  };

  // handleKeyPressSoTK = e => {
  //   if (e.keyCode == 13 || e.keyCode == 9) {
  //     let SO_TK = e.target.value;
  //     query(
  //       "get_voucher_info",
  //       this.props.currentUser,
  //       "tax_declaration",
  //       SO_TK
  //     )
  //       .then(res => {
  //         console.log(res);
  //         this.setState({
  //           SO_CT_HQ: res.SO_CT,
  //           MA_HQ_PH: res.MA_HQ_PH,
  //           TEN_HQ_PH: res.TEN_HQ_PH,
  //           MA_HQ: res.MA_HQ,
  //           TEN_HQ: res.TEN_HQ,
  //           MA_DV: res.MA_DV,
  //           TEN_DV: res.TEN_DV,
  //           MA_LH: res.MA_LH,
  //           TEN_LH: res.TEN_LH,
  //           MA_KB: res.MA_KB,
  //           TEN_KB: res.TEN_KB,
  //           MA_HTVCHH: res.MA_HTVCHH,
  //           TEN_HTVCHH: res.TEN_HTVCHH,
  //           TKKB: res.TKKB,
  //           DUNO_TO: res.DUNO_TO,
  //           LOAI_THUE: res.LOAI_THUE,
  //           NGAY_DK: moment(res.NGAY_DK, "DD/MM/YYYY"),
  //           NGAY_HL: moment(res.NGAY_HL, "DD/MM/YYYY"),
  //           MA_CHUONG: res.MA_CHUONG
  //         });
  //       })
  //       .catch(err => console.log(err));
  //   }
  // };

  handleClickBack = () => {
    this.props.history.goBack();
  };

  render() {
    let showAlert = null;
    let infoSummary = null;

    if (this.state.isShow) {
      if (this.state.message === "SUCCESS") {
        showAlert = (
          <Alert bsStyle="success">
            <strong>Chúc mừng!</strong> Bạn đã tạo mới dữ liệu thành công.
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      } else {
        showAlert = (
          <Alert bsStyle="danger">
            <strong>Lỗi!</strong> Xin vui lòng kiểm tra lại. {this.state.message}
            <p className="dismiss-alert">
              <span onClick={this.handleDismissAlert}>x</span>
            </p>
          </Alert>
        );
      }
    }

    if (!this.state.loading) {
      infoSummary = (
        <form>
          <h3>Giấy nộp thuế</h3>
          <div className="row">
            <Col sm={2}>
              <FormGroup>
                <span>Số TK</span>
                <FormControl
                  type="text"
                  value={this.state.SO_TK}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_TK: e.target.value })}
                  onKeyDown={this.handleKeyPressSoTK}
                  disabled="true"
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <span>Ngày ĐK</span>
              <DatePicker
                dateFormat="DD/MM/YYYY"
                selected={this.state.NGAY_HL}
                onChange={date => this.handleChangeNgayHL(date)}
                className="form-control"
                onChangeRaw={event => this.myCustomFunction(event.target.value)}
                disabled
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              {/* <FormGroup> */}
              <span>Nơi phát hành chứng từ</span>
              <div className="row">
                <Col sm={4}>
                  <FormControl
                    type="text"
                    value={this.state.MA_HQ_PH}
                    placeholder="Nhập"
                    onChange={e => this.setState({ MA_HQ_PH: e.target.value })}
                    onKeyDown={this.handleKeyPressMaHQPH}
                    disabled="true"
                  />
                </Col>
                <Col sm={8}>
                  <FormGroup controlId="formControlsSelect">
                    <FormControl
                      inputRef={el => (this.inputEl = el)}
                      componentClass="select"
                      placeholder="select"
                      value={this.state.MA_HQ_PH}
                      onChange={this.handleChangeTENHQPH}
                      disabled="true"
                    >
                      <option value="">Chi cục HQ CK ...</option>
                      {this.state.TenHQPH.map(c => (
                        <option key={c.code} value={c.code}>
                          {c.name}
                        </option>
                      ))}
                    </FormControl>
                  </FormGroup>
                </Col>
              </div>
              {/* </FormGroup> */}
            </Col>
            <Col sm={2}>
              <FormGroup>
                <span>Số chứng từ</span>
                <FormControl
                  type="text"
                  value={this.state.SO_CT_HQ}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_CT_HQ: e.target.value })}
                  disabled="true"
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <span>Ngày HL</span>
              <DatePicker
                dateFormat="DD/MM/YYYY"
                selected={this.state.NGAY_HL}
                onChange={date => this.handleChangeNgayHL(date)}
                className="form-control"
                onChangeRaw={event => this.myCustomFunction(event.target.value)}
                disabled
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Nơi mở tờ khai</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_HQ}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_HQ}
                        onChange={this.handleChangeTENHQPH}
                        disabled="true"
                      >
                        <option value="">Chi cục HQ CK ...</option>
                        {this.state.TenHQPH.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Loại hình XNK</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_LH}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_LH}
                        onChange={this.handleChangeTENHQPH}
                        disabled="true"
                      >
                        <option value="">Chi cục HQ CK ...</option>
                        {this.state.LHXNK.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Đơn vị xuất nhập khẩu</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_DV}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_DV}
                        onChange={this.handleChangeTENHQPH}
                        disabled="true"
                      >
                        <option value="">Chi cục HQ CK ...</option>
                        {this.state.TenDVXNK.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
            <Col sm={2}>
              <span>Mã chương</span>
              <FormControl
                type="text"
                value={this.state.MA_CHUONG}
                disabled="true"
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={8}>
              <FormGroup>
                <span>Kho bạc</span>
                <div className="row">
                  <Col sm={4}>
                    <FormControl
                      type="text"
                      value={this.state.MA_KB}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_KB: e.target.value })}
                      onKeyDown={this.handleKeyPressMaKB}
                      disabled="true"
                    />
                  </Col>
                  <Col sm={8}>
                    <FormGroup controlId="formControlsSelect">
                      <FormControl
                        inputRef={el => (this.inputEl = el)}
                        componentClass="select"
                        placeholder="select"
                        value={this.state.MA_KB}
                        onChange={this.handleChangeKhoBac}
                        disabled="true"
                      >
                        <option value="">...</option>
                        {this.state.KhoBac.map(c => (
                          <option key={c.code} value={c.code}>
                            {c.name}
                          </option>
                        ))}
                      </FormControl>
                    </FormGroup>
                  </Col>
                </div>
              </FormGroup>
            </Col>
            <Col sm={2}>
              <span>Tài khoản KB</span>
              <FormControl
                type="text"
                value={this.state.TKKB}
                disabled="true"
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={3}>
              <span>Sắc thuế</span>
              {/* <div className="row">
                <Col sm={12}>
                  <FormControl
                    type="text"
                    value={this.state.LOAI_THUE}
                    disabled="true"
                  />
                </Col>
              </div> */}
            </Col>
            <Col sm={3}>
              <span>Số tiền</span>
              {/* <div className="row">
                <Col sm={12}>
                  <FormControl
                    type="text"
                    value={this.state.DUNO_TO}
                    disabled="true"
                  />
                </Col>
              </div> */}
            </Col>
          </div>

          {this.state.ST.map(obj => {
            return (
              <div className="row">
                <Col sm={3}>
                  <FormGroup controlId="formControlsSelect">
                    <FormControl
                      inputRef={el => (this.inputEl = el)}
                      componentClass="select"
                      placeholder="select"
                      value={obj.ST_KEY}
                      disabled
                    >
                      <option value="">Chọn giá trị ...</option>
                      {this.state.SacThue.map(c => (
                        <option key={c.code} value={c.prefix}>
                          {c.name}
                        </option>
                      ))}
                    </FormControl>
                  </FormGroup>
                </Col>
                <Col sm={2}>
                  <NumberFormat
                    value={obj.ST_MONEY || 0}
                    thousandSeparator={true}
                    className="form-control money"
                    disabled
                  />
                </Col>
              </div>
            );
          })}

          <div className="row">
            <Col sm={3}>
              <span>Tổng</span>
            </Col>
            <Col sm={2}>
              <NumberFormat
                value={this.state.DUNO_TO || 0}
                thousandSeparator={true}
                className="form-control money"
                disabled
              />
            </Col>
          </div>

          <hr />

          <div className="row">
            <Col sm={2}>
              <FormGroup>
                <span>Số chứng từ</span>
                <FormControl
                  type="text"
                  value={this.state.SO_CT}
                  placeholder="Nhập"
                  onChange={e => this.setState({ SO_CT: e.target.value })}
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <FormGroup>
                <span>Kí hiệu chứng từ</span>
                <FormControl
                  type="text"
                  value={this.state.KHCT}
                  placeholder="Nhập"
                  onChange={e => this.setState({ KHCT: e.target.value })}
                />
              </FormGroup>
            </Col>
            <Col sm={2}>
              <span>Ngày lập</span>
              <DatePicker
                dateFormat="DD/MM/YYYY"
                selected={this.state.NGAY_LAP}
                onChange={date => this.handleChangeNgayLAP(date)}
                className="form-control"
                onChangeRaw={event => this.myCustomFunction(event.target.value)}
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={12}>
              <FormGroup>
                <span>Ngân hàng</span>
                <div className="row">
                  <Col sm={2}>
                    <FormControl
                      type="text"
                      value={this.state.MA_NH}
                      placeholder="Nhập"
                      onChange={e => this.setState({ MA_NH: e.target.value })}
                      onKeyDown={this.handleKeyPressMaHQ}
                    />
                  </Col>
                  <Col sm={6}>
                    {/* <FormGroup controlId="formControlsSelect"> */}
                    <FormControl
                      inputRef={el => (this.inputEl = el)}
                      componentClass="select"
                      placeholder="select"
                      value={this.state.MA_NH}
                      onChange={this.handleChangeTenNH}
                    >
                      <option value="">...</option>
                      {this.state.TenNH.map(c => (
                        <option key={c.code} value={c.code}>
                          {c.name}
                        </option>
                      ))}
                      >
                    </FormControl>
                    {/* </FormGroup> */}
                  </Col>
                </div>
              </FormGroup>
            </Col>
          </div>

          <div className="row">
            <Col sm={2}>
              {/* <FormGroup> */}
              <span>Số tài khoản</span>
            </Col>
            <Col sm={2}>
              <FormGroup controlId="formControlsSelect">
                <FormControl
                  inputRef={el => (this.inputEl = el)}
                  componentClass="select"
                  placeholder="select"
                  value={this.state.MA_TK_PH}
                  onChange={e => this.setState({ MA_TK_PH: e.target.value })}
                >
                  <option value="000001">000001</option>
                  <option value="000002">000002</option>
                  <option value="000003">000003</option>
                  >
                </FormControl>
              </FormGroup>
              {/* </FormGroup> */}
            </Col>
            <Col sm={2}>
              <span>Tên tài khoản</span>
            </Col>
            <Col sm={4}>
              <FormControl
                type="text"
                value={this.state.TEN_TK_PH}
                placeholder="Nhập"
                onChange={e => this.setState({ TEN_TK_PH: e.target.value })}
                onKeyDown={this.handleKeyPressMaHQ}
              />
            </Col>
          </div>

          <div className="row">
            <Col sm={2}>
              <span>Số tiền</span>
            </Col>
            <Col sm={2}>
              <NumberFormat
                value={this.state.SO_TIEN || 0}
                thousandSeparator={true}
                className="form-control money"
                onValueChange={values => {
                  const { formattedValue, value, floatValue } = values;
                  this.setState({ SO_TIEN: floatValue || 0 });
                }}
              />
            </Col>
          </div>

          <hr />

          <div className="ButtonGroup">
            <Button bsStyle="primary" onClick={this.handleSubmit}>
              Chuyển
            </Button>
            <Button bsStyle="primary" onClick={this.handleClickBack}>
              Thoát
            </Button>
          </div>
        </form>
      );
    } else {
      infoSummary = <Spinner />;
    }

    return (
      <div className="Declaration">
        {showAlert}
        {infoSummary}
      </div>
    );
  }
}

export default GiayNopThue;
