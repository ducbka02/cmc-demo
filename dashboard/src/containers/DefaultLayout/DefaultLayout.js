import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { Container } from "reactstrap";

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav
} from "@coreui/react";
// sidebar nav config
import {
  navigationCompany,
  navigationBank,
  navigationCustoms,
  navigationTreasury
} from "../../_nav";
// routes config
import routes from "../../routes";
import DefaultAside from "./DefaultAside";
import DefaultFooter from "./DefaultFooter";
import DefaultHeader from "./DefaultHeader";

import { PrivateRoute } from "../../components/Common/PrivateRoute";
import axios from "axios";
import { fakeAuth } from "../../components/Common/FakeAuth";
import { SERVER_URL, SOCKET_URL } from "../../api_server";
import openSocket from "socket.io-client";

let socket;

class DefaultLayout extends Component {
  state = {
    currentUser: {
      username: "",
      role: "",
      channel: "",
      unit: ""
    },
    countNotify: 0
  };

  componentDidMount() {
    const isAuthenticated = localStorage.getItem("isAuthenticated");
    if (isAuthenticated) {
      this.setState({
        currentUser: {
          username: localStorage.getItem("username"),
          role: localStorage.getItem("role"),
          unit: localStorage.getItem("unit")
        }
      });

      const role = localStorage.getItem("role");

      // // TODO: Socket
      // socket = openSocket(SOCKET_URL);

      socket = openSocket(SOCKET_URL);

      // console.log("SOCKET", socket);

      socket.emit("currentUser", JSON.stringify({
          username: localStorage.getItem("username"),
          role: localStorage.getItem("role"),
          unit: localStorage.getItem("unit")
      }));

      socket.on("userList", function(data) {
        console.log("userList", data);
      });

      // Signout
      socket.on("exit", function(data) {
        console.log("userList", data);
      });
      
      // Receive message from Server
      if(role == "company") {
        if(window.location.pathname == "/") {
          this.props.history.push("/base/congty");
        }
        socket.on("company-declaration", function(data) {
          console.log("Got new message:", data.message);
          window.notifyMe("haiquan", data.message, window.location.origin + "/base/congty");
        });

        this.receivedUnreadMessages("company", "haiquan", "/base/congty");

        // socket.emit("company-receive-unread-message", 
        //         {unit: localStorage.getItem("unit")}, 
        //         (data) => {
        //           if(data !== null) {
        //             const unReadMessages = JSON.parse(data);
        //             if(unReadMessages.unit == localStorage.getItem("unit") && unReadMessages.status == "unread") {
        //               window.notifyMe("haiquan", unReadMessages.message, window.location.origin + "/base/congty");
        //               socket.emit("company-received-messages", '');
        //             } else {
        //               console.log("khong trung Unit code");
        //             }
        //           }
        //         }
        // );
      } else if(role == "bank") {
        if(window.location.pathname == "/") {
          this.props.history.push("/base/nganhang");
        }
        socket.on("bank-giaynopthue", function(data) {
          console.log("Got new message:", data.message);
          window.notifyMe("company", data.message, window.location.origin + "/base/nganhang");
        });

        this.receivedUnreadMessages("bank", "nganhang", "/base/nganhang");
      } else if(role == "customs") {
        if(window.location.pathname == "/") {
          this.props.history.push("/base/haiquan");
        }
        socket.on("customs-duyetthanhtoan", function(data) {
          console.log("Got new message:", data.message);
          window.notifyMe("bank", data.message, window.location.origin + "/base/haiquan");
        });

        socket.on("customs-xacnhannopthue", function(data) {
          console.log("Got new message:", data.message);
          window.notifyMe("treasury", data.message ,window.location.origin + "/base/haiquan");
        });

        this.receivedUnreadMessages("customs", "nganhang", "/base/haiquan");
        this.receivedUnreadMessages("customs", "khobac", "/base/haiquan");
      } else {
        if(window.location.pathname == "/") {
          this.props.history.push("/base/khobac");
        }
        // console.log("Role Treasury");
        socket.on("treasury-xacnhanhtnvt", function(data) {
          console.log("Got new message:", data.message);
          window.notifyMe("customs", data.message, window.location.origin + "/base/khobac");
        });

        this.receivedUnreadMessages("treasury", "haiquan", "/base/khobac");
      }

    }
    // if(isAuthenticated) {
    //   const username = localStorage.getItem('username');
    //   axios.get(`${SERVER_URL}/api/userinfo?username=${username}`)
    //    .then(res => {
    //      this.setState({ currentUser: {
    //        username: res.data[0].username,
    //        role: res.data[0].role,
    //        channel: res.data[0].channel,
    //        unit: res.data[0].unit,
    //      }})
    //    })
    //    .catch(err => console.log(err));
    // }
  }

  receivedUnreadMessages = (roleName, fromRole, navigateLink) => {
    socket.emit(roleName + "-receive-unread-message", 
        {unit: localStorage.getItem("unit")}, 
        (data) => {
          if(data !== null) {
            const unReadMessages = JSON.parse(data);
            if(unReadMessages.unit == localStorage.getItem("unit") && unReadMessages.status == "unread") {
              this.setState((prevState) => ({ countNotify: prevState.countNotify + 1 }));
              window.notifyMe(fromRole, unReadMessages.message, window.location.origin + navigateLink);
              socket.emit(roleName + "-received-messages", unReadMessages.unit);
            } else {
              console.log("khong trung Unit code");
            }
          }
        }
    );
  }

  render() {
    let navigation = null;

    switch (this.state.currentUser.role) {
      case "company":
        navigation = navigationCompany;
        break;
      case "bank":
        navigation = navigationBank;
        break;
      case "customs":
        navigation = navigationCustoms;
        break;
      case "treasury":
        navigation = navigationTreasury;
        break;
      default:
        navigation = navigationBank;
        break;
    }

    return (
      <div className="app">
        <AppHeader fixed>
          <DefaultHeader currentUser={this.state.currentUser.username} socket={socket} countNotify={this.state.countNotify} />
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <AppSidebarNav navConfig={navigation} {...this.props} />
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            {/* <AppBreadcrumb appRoutes={routes} /> */}
                {/* {routes.map((route, idx) => {
                    return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                        <route.component {...props} />
                      )} />)
                      : (null);
                  },
                )} */}
                {routes.map((route, idx) => {
                  return route.component ? (
                    <div>
                      <Container fluid>
                        <Switch>
                          <PrivateRoute
                            key={idx}
                            path={route.path}
                            component={route.component}
                            currentUser={this.state.currentUser.username}
                            currentUnit={this.state.currentUser.unit}
                            socket={socket}
                          />
                        </Switch>
                      </Container>  
                    </div>
                  ) : null;
                })}
                {/* <Redirect from="/" to="/dashboard" /> */}

          </main>
          <AppAside fixed hidden>
            <DefaultAside />
          </AppAside>
        </div>
        {/* <AppFooter>
          <DefaultFooter />
        </AppFooter> */}
      </div>
    );
  }
}

export default DefaultLayout;
