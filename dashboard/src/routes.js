import React from 'react';
import Loadable from 'react-loadable'

import DefaultLayout from './containers/DefaultLayout';
import Declaration from './components/Declaration/Declaration';
import TraCuuTK from './components/Declaration/TraCuuTK';
import GiayNopThue from './components/Declaration/GiayNopThue';
import DuyetThanhToan from './components/Declaration/DuyetThanhToan';
import XacNhanHTVNT from './components/Declaration/XacNhanHTVNT';
import XacNhanNopThue from './components/Declaration/XacNhanNopThue';
import Haiquan from './components/Haiquan/Haiquan';
import Congty from './components/Congty/Congty';
import Nganhang from './components/Nganhang/Nganhang';
import Khobac from './components/Khobac/Khobac';

function Loading() {
  return <div>Loading...</div>;
}

const Dashboard = Loadable({
  loader: () => import('./views/Dashboard'),
  loading: Loading,
});

// const Declaration = Loadable({
//   loader: () => import('./views/Base/Declaration'),
//   loading: Loading,
// });

//
const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/base/declaration', exact: true, name: 'Declaration', component: Declaration },
  { path: '/base/tracuutk', exact: true, name: 'TraCuuTK', component: TraCuuTK },
  { path: '/base/giaynopthue', exact: true, name: 'GiayNopThue', component: GiayNopThue },
  { path: '/base/duyetthanhtoan', exact: true, name: 'DuyetThanhToan', component: DuyetThanhToan },
  { path: '/base/xacnhanhtvnt', exact: true, name: 'XacNhanHTVNT', component: XacNhanHTVNT },
  { path: '/base/xacnhannopthue', exact: true, name: 'XacNhanNopThue', component: XacNhanNopThue },
  { path: '/base/haiquan', exact: true, name: 'Haiquan', component: Haiquan },
  { path: '/base/congty', exact: true, name: 'Congty', component: Congty },
  { path: '/base/nganhang', exact: true, name: 'Nganhang', component: Nganhang },
  { path: '/base/khobac', exact: true, name: 'Khobac', component: Khobac },
];

export default routes;
