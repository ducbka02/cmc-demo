import React, { Component } from "react";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  InputGroupButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  FormGroup,
  FormControl
} from "reactstrap";
import axios from "axios";
import { SERVER_URL, SOCKET_URL } from "../../../api_server";
import Spinner from "../../../components/UI/Spinner/Spinner";
// import openSocket from "socket.io-client";

class Register extends Component {
  state = {
    username: "",
    password: "",
    email: "",
    role: "",
    channel: "",
    unit: "",
    dropdownOpen: false,
    dropDownValue: "Select Role",
    dropdownOpenUnit: false,
    dropDownValueUnit: "Select Unit",
    units: [],
    banks: [],
    companies: [],
    customs: [],
    treasuries: [],
    alertMessage: "",
    loading: false
  };

  componentDidMount() {
    this.getParallel();

    // const socket = openSocket(SOCKET_URL);

    // // Receive message from Server
    // socket.on("announcements", function(data) {
    //   console.log("Got announcement:", data.message);
    // });

    // // Send message to Server
    // socket.emit("event", { message: "Hey, I have an important message!" });

    // axios
    //   .get(`${SERVER_URL}/api/choicelist?choicelistType=customs_unit`)
    //   .then(res => {
    //     console.log(res);
    //     this.setState({
    //       customs: res.data
    //     });
    //   })
    //   .catch(err => console.log(err));

    // axios
    //   .get(`${SERVER_URL}/api/choicelist?choicelistType=bank_list`)
    //   .then(res => {
    //     console.log(res);
    //     this.setState({
    //       banks: res.data
    //     });
    //   })
    //   .catch(err => console.log(err));

    // axios
    //   .get(`${SERVER_URL}/api/choicelist?choicelistType=company_list`)
    //   .then(res => {
    //     console.log(res);
    //     this.setState({
    //       companies: res.data
    //     });
    //   })
    //   .catch(err => console.log(err));

    // axios
    //   .get(`${SERVER_URL}/api/choicelist?choicelistType=treasury_list`)
    //   .then(res => {
    //     console.log(res);
    //     this.setState({
    //       treasuries: res.data
    //     });
    //   })
    //   .catch(err => console.log(err));
  }

  getParallel = async () => {
    const urls = [
      "/api/choicelist?choicelistType=customs_unit",
      "/api/choicelist?choicelistType=bank_list",
      "/api/choicelist?choicelistType=company_list",
      "/api/choicelist?choicelistType=treasury_list"
    ];

    //transform requests into Promises, await all
    try {
      const promises = urls.map(url =>
        axios.get(`${SERVER_URL}` + url).then(res => res.data)
      );
      // console.log(promises);
      // Promise.all(promises).then(([res1,res2,res3,res4]) => {})
      await Promise.all(promises).then(results => {
        // console.log(results);
        this.setState({
          customs: results[0],
          banks: results[1],
          companies: results[2],
          treasuries: results[3]
        });
      });
    } catch (err) {
      throw err;
    }
  };

  handleRegister = event => {
    event.preventDefault();

    const data = {
      username: this.state.username,
      password: this.state.password,
      email: this.state.email,
      role: this.state.role,
      channel: "mychannel",
      unit: this.state.unit
    };

    this.setState({ loading: true });
    axios
      .post(`${SERVER_URL}/api/signup`, data)
      .then(res => {
        // console.log(res);
        // console.log(this.props);
        if (res.data.code === 1) {
          this.setState({ alertMessage: "Đăng ký thành công!" });
          this.props.history.push("/login");
        } else {
          console.log(res);
          this.setState({ alertMessage: "Đã có lỗi xảy ra!" });
          this.setState({ loading: false });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  toggleDropDown = () => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  };

  toggleDropDownUnit = () => {
    this.setState(prevState => ({
      dropdownOpenUnit: !prevState.dropdownOpenUnit
    }));
  };

  selectDropdownItem = event => {
    // console.log(event.currentTarget.value);
    // console.log(event.currentTarget.textContent);
    const userRole = event.currentTarget.value;
    this.setState({
      dropDownValue: event.currentTarget.textContent,
      role: userRole,
      units: [],
      dropDownValueUnit: "Select Unit"
    });

    switch (userRole) {
      case "bank":
        console.log("bank", this.state.banks);
        this.setState({ units: this.state.banks });
        break;
      case "customs":
        this.setState({ units: this.state.customs });
        break;
      case "company":
        this.setState({ units: this.state.companies });
        break;
      case "treasury":
        this.setState({ units: this.state.treasuries });
        break;
      default:
        break;
    }
  };

  selectDropdownItemUnit = event => {
    console.log(event.currentTarget.value);
    // console.log(event.currentTarget.textContent);
    const userRole = event.currentTarget.value;
    this.setState({
      dropDownValueUnit: event.currentTarget.textContent,
      unit: event.currentTarget.value
    });
  };

  handleChangeUnits = e => {
    this.setState({
      unit: e.target.value
    });
  };

  render() {
    let renderView = null;

    if (!this.state.loading) {
      renderView = (
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="6">
                <Card className="mx-4">
                  <CardBody className="p-4">
                    <h1>Register</h1>
                    <p className="text-muted">Create your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="text"
                        placeholder="Username"
                        value={this.state.username}
                        onChange={e =>
                          this.setState({ username: e.target.value })}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="email"
                        placeholder="Email"
                        value={this.state.email}
                        onChange={e => this.setState({ email: e.target.value })}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        type="password"
                        placeholder="Password"
                        value={this.state.password}
                        onChange={e =>
                          this.setState({ password: e.target.value })}
                      />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-home" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <InputGroupButtonDropdown
                        addonType="append"
                        isOpen={this.state.dropdownOpen}
                        toggle={this.toggleDropDown}
                      >
                        <DropdownToggle caret>
                          {this.state.dropDownValue}
                        </DropdownToggle>
                        <DropdownMenu>
                          <DropdownItem
                            onClick={this.selectDropdownItem}
                            value="bank"
                          >
                            Ngân hàng
                          </DropdownItem>
                          <DropdownItem
                            onClick={this.selectDropdownItem}
                            value="company"
                          >
                            Doanh nghiệp
                          </DropdownItem>
                          <DropdownItem
                            onClick={this.selectDropdownItem}
                            value="customs"
                          >
                            Hải quan
                          </DropdownItem>
                          <DropdownItem
                            onClick={this.selectDropdownItem}
                            value="treasury"
                          >
                            Kho bạc
                          </DropdownItem>
                        </DropdownMenu>
                      </InputGroupButtonDropdown>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-home" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <InputGroupButtonDropdown
                        addonType="append"
                        isOpen={this.state.dropdownOpenUnit}
                        toggle={this.toggleDropDownUnit}
                      >
                        <DropdownToggle caret>
                          {this.state.dropDownValueUnit}
                        </DropdownToggle>
                        <DropdownMenu>
                          {this.state.units.map(c => (
                            <DropdownItem
                              key={c.code}
                              value={c.code}
                              onClick={this.selectDropdownItemUnit}
                            >
                              {c.name}
                            </DropdownItem>
                          ))}
                        </DropdownMenu>
                      </InputGroupButtonDropdown>
                    </InputGroup>

                    <Button color="success" block onClick={this.handleRegister}>
                      Create Account
                    </Button>
                    <span style={{ color: "red" }}>
                      {this.state.alertMessage}
                    </span>
                  </CardBody>
                  {/* <CardFooter className="p-4">
                  <Row>
                    <Col xs="12" sm="6">
                      <Button className="btn-facebook" block>
                        <span>facebook</span>
                      </Button>
                    </Col>
                    <Col xs="12" sm="6">
                      <Button className="btn-twitter" block>
                        <span>twitter</span>
                      </Button>
                    </Col>
                  </Row>
                </CardFooter> */}
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      );
    } else {
      renderView = <Spinner />;
    }

    return <div>{renderView}</div>;
  }
}

export default Register;
