module.export =

// Dummy Data
{
	fakeData : [
	{
		id: 100,
		name: "GD 001",
		total_money: 1000
	},
	{
		id: 101,
		name: "GD 002",
		total_money: 2000
	},
	{
		id: 102,
		name: "GD 003",
		total_money: 3000
	}
],

fakeDataCompany: [
	{
		taxId: 111,
		companyName: "Công ty phần mềm CMC",
		bankId: 1,
		bankName: "Ngân hàng Vietcombank",
		accountNumb: 111111,
		accountName: 'Nguyễn Văn A'
	},
	{
		taxId: 222,
		companyName: "Công ty phần mềm CMC",
		bankId: 2,
		bankName: "Ngân hàng Vietinbank",
		accountNumb: 222222,
		accountName: 'Nguyễn Văn B'
	}
]

};
